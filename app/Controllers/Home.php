<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        return view('welcome_message');
    }
    
    public function phpinfo(){
        phpinfo();
    }
    
    public function contar(){
        for($i=1;$i<10;$i++){
            echo $i;
        }
    }
}
